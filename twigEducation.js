/**
 *  Returns an Array divided to n equal parts
 * If arr % num !== 0 the remainder is added to  the last array   
 * @param  arr  lenght => 0   
 * @param  num positive integer 
 */


function groupArrayElements(arr, num){
    let result = [];
    const arrayCopy = [...arr] // copay of the original array because splice would mutate that array 
    let sizeOfParts =  Math.round(arr.length / num);
    let remainder = arr.length % num
    if (sizeOfParts * (num -1) > arr.length ) {    // check if Math.round work well 
        sizeOfParts = Math.floor(arr.length / num) 
    }
     for(let i=0; i< num-1; i++){
        result.push(arrayCopy.splice(0, sizeOfParts))
     }
    
     result.push(arrayCopy.splice(0)) // the last part is the remainder of the array  
      return result;
  }  

